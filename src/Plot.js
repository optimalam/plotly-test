import React, { Component } from 'react';
import Plotly from 'plotly.js';
import { v4 } from 'node-uuid';

class Plot extends Component {
  componentDidMount() {
    const trace1 = {
      x: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      y: [0, 3, 6, 4, 5, 2, 3, 5, 4],
      mode: 'lines',
      name:'Plot 1'
    };
    const trace2 = {
      x: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      y: [0, 4, 7, 8, 3, 6, 3, 3, 4],
      mode: 'lines',
      name:'Plot 2'
    };
    const trace3 = {
      x: [0, 1, 2, 3, 4, 5, 6, 7, 8],
      y: [0, 5, 3, 10, 5.33, 2.24, 4.4, 5.1, 7.2],
      mode: 'lines',
      name:'Plot 3'
    };

    const data = [trace1, trace2, trace3];

    const defaultPlotlyOptions = {
      displaylogo: false,
      modeBarButtonsToRemove: [
        'sendDataToCloud', 'hoverCompareCartesian', 'hoverClosestCartesian'
      ],
      editable: false,
      displayModeBar: true,
      scrollZoom: false
    };

    const layout = {
    	showlegend: true,
    	legend: {
        orientation: 'h'
      },
      title: 'Plotly Test Horizontal Legend',
      xaxis: {
        title: 'X axis'
      },
      yaxis: {
        title: 'Y axis'
      }
    };

    Plotly.newPlot(this.plotGlobalId, data, layout, defaultPlotlyOptions);
  }
  render() {
    // give this particular instance a new id that is globally unique for the div
    // so it can be used multiple times.
    this.plotGlobalId = v4();

    return (
      <div id={this.plotGlobalId}/>
    )
  }
}

export default Plot;
